﻿using UnityEngine;

public class playercollision : MonoBehaviour
{
    public playermovement movement;

    void OnCollisionEnter(Collision colinfo)
    {
        if (colinfo.collider.tag == "obstical")
        {
            //Debug.Log("we hit");
            movement.enabled = false;
            FindObjectOfType<GameManager>().EndGame(); 
             
        }
    }
}
   