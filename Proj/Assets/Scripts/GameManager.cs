﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameEnd = false;
    public float restartDelay = 1f;
    public GameObject clUI;
    public void CompleteLevel()
    {
        clUI.SetActive(true); 
    }
    public void EndGame() 
    {
        if (gameEnd == false)
        {
            gameEnd = true;
            //Debug.Log("game over");
            Invoke("Restart", restartDelay);
        }
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}  
 